package com.soniccomputing.golfscoretracker.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.soniccomputing.golfscoretracker.R
import com.soniccomputing.golfscoretracker.databinding.EnterHoleResultBinding
import com.soniccomputing.golfscoretracker.model.GolfHole
import com.soniccomputing.golfscoretracker.tracker.HoleResult
import com.soniccomputing.golfscoretracker.tracker.PlayerScore
import com.soniccomputing.golfscoretracker.tracker.TotalPlayerScore
import com.soniccomputing.golfscoretracker.ui.EnterPlayerScoreAdapter
import com.soniccomputing.golfscoretracker.ui.HoleResultAdapter
import com.soniccomputing.golfscoretracker.ui.PlayerResultAdapter
import com.soniccomputing.golfscoretracker.viewmodel.TrackerViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class EnterHoleResultFragment :
    Fragment(R.layout.enter_hole_result) {
    private val trackerViewModel: TrackerViewModel by sharedViewModel()
    private lateinit var binding: EnterHoleResultBinding
    private lateinit var currentHole: GolfHole
    private var holeResults = mutableListOf<HoleResult>()
    private var totalPlayerScores = mutableListOf<TotalPlayerScore>()

    companion object {
        const val ARG_HOLE_NUM = "hole_num"

        fun getInstance(holeNum: Int): Fragment {
            val fragment = EnterHoleResultFragment()
            val bundle = Bundle()
            bundle.putInt(ARG_HOLE_NUM, holeNum + 1)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        EnterHoleResultBinding.inflate(inflater, container, false).apply { binding = this }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val holeNum = requireArguments().getInt(ARG_HOLE_NUM)

        currentHole = trackerViewModel.getHoleInfo(holeNum)
        updateTitle()

        val totalResultAdapter = PlayerResultAdapter(layoutInflater)
        binding.scoresList.apply {
            adapter = totalResultAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(
                DividerItemDecoration(
                    activity,
                    DividerItemDecoration.VERTICAL
                )
            )
        }
        trackerViewModel.players.forEach { player ->
            totalPlayerScores.add(TotalPlayerScore(player, 0, 0, 0))
        }
        totalResultAdapter.submitList(totalPlayerScores)

        val adapter = EnterPlayerScoreAdapter(layoutInflater)
        binding.enterPlayersList.apply {
            setAdapter(adapter)
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(
                DividerItemDecoration(
                    activity,
                    DividerItemDecoration.VERTICAL
                )
            )
        }
        val playerScores = trackerViewModel.players.map {
            PlayerScore(it)
        }.toMutableList()
        adapter.submitList(playerScores)

        val holeResultAdapter = HoleResultAdapter(layoutInflater)
        binding.holeResultList.apply {
            setAdapter(holeResultAdapter)
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(
                DividerItemDecoration(
                    activity,
                    DividerItemDecoration.VERTICAL
                )
            )
        }
        holeResultAdapter.submitList(holeResults)

        binding.enterResult.setOnClickListener {
            val lastRow = binding.enterPlayersList[playerScores.size - 1]
            val lastRowEditText = lastRow.findViewById<EditText>(R.id.score)
            if (lastRowEditText.text.toString().isEmpty()) {
                return@setOnClickListener
            }
            playerScores[playerScores.size - 1].score =
                lastRowEditText.text.toString().toInt()
            trackerViewModel.enterHoleResult(playerScores, currentHole.number)
        }

        trackerViewModel.holeResult.observeForever {
            holeResults.add(0, it)
            holeResultAdapter.notifyItemInserted(0)
            totalPlayerScores.clear()
            trackerViewModel.players.forEach { player ->
                val count = trackerViewModel.getWinningCountForPlayer(player)
                totalPlayerScores.add(TotalPlayerScore(player, count, 0, 0))
            }
            totalResultAdapter.notifyDataSetChanged()
        }
    }

    private fun updateTitle() {
        val title = getString(R.string.enter_hole_result_title, currentHole.number, currentHole.par)
        binding.title.text = title
    }
}