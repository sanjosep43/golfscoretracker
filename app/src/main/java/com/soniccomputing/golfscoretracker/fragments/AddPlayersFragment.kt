package com.soniccomputing.golfscoretracker.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.soniccomputing.golfscoretracker.R
import com.soniccomputing.golfscoretracker.databinding.AddPlayersBinding
import com.soniccomputing.golfscoretracker.model.Player
import com.soniccomputing.golfscoretracker.ui.PlayerAdapter
import com.soniccomputing.golfscoretracker.viewmodel.TrackerViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class AddPlayersFragment :
    Fragment(R.layout.add_players) {
    private val trackerViewModel: TrackerViewModel by sharedViewModel()
    private lateinit var binding: AddPlayersBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = AddPlayersBinding.inflate(inflater, container, false).apply { binding = this }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = PlayerAdapter(layoutInflater) { model ->
            trackerViewModel.players.remove(model)
        }
        binding.apply {
            addPlayer.setOnClickListener {
                if (name.text.isNotEmpty() && handicap.text.isNotEmpty()) {
                    trackerViewModel.players.add(
                        Player(
                            name = name.text.toString(),
                            handicap = Integer.parseInt(handicap.text.toString())
                        )
                    )
                    adapter.notifyDataSetChanged()
                    name.text.clear()
                    handicap.text.clear()
                }
            }
            done.setOnClickListener { findNavController().popBackStack() }
        }

        binding.playerList.apply {
            setAdapter(adapter)
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(
                DividerItemDecoration(
                    activity,
                    DividerItemDecoration.VERTICAL
                )
            )
        }
        adapter.submitList(trackerViewModel.players)
    }
}