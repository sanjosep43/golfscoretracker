package com.soniccomputing.golfscoretracker.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.soniccomputing.golfscoretracker.R

class HomeFragment : Fragment(R.layout.home) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<Button>(R.id.start_button).setOnClickListener {
            findNavController().navigate(R.id.setupFragment)
        }
    }
}