package com.soniccomputing.golfscoretracker.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.soniccomputing.golfscoretracker.R
import com.soniccomputing.golfscoretracker.viewmodel.TrackerViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class SetupFragment : Fragment(R.layout.setup) {
    private val trackerViewModel: TrackerViewModel by sharedViewModel()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<TextView>(R.id.course).setOnClickListener {
            findNavController().navigate(R.id.navigate_to_course_selection)
        }
        view.findViewById<TextView>(R.id.game).setOnClickListener {
            findNavController().navigate(R.id.navigate_to_game_selection)
        }
        view.findViewById<Button>(R.id.add_players).setOnClickListener {
            findNavController().navigate(R.id.navigate_to_add_players)
        }
        view.findViewById<Button>(R.id.start_game).setOnClickListener {
            findNavController().navigate(R.id.navigate_to_game_play)
        }
    }

    override fun onResume() {
        super.onResume()
        if(trackerViewModel.isCurrentCourseInitialized()) {
            trackerViewModel.currentCourse.let {
                view?.findViewById<TextView>(R.id.course)?.text = it.name
            }
        }
        if(trackerViewModel.isCurrentGameInitialized()) {
            trackerViewModel.currentGame.let {
                view?.findViewById<TextView>(R.id.game)?.text = it.name
            }
        }
        if(trackerViewModel.players.size > 0) {
            var result = "Players: "
            trackerViewModel.players.forEach {
                result += it.name + ","
            }
            result = result.removeSuffix(",")
            view?.findViewById<TextView>(R.id.players_summary)?.text = result
        } else {
            view?.findViewById<TextView>(R.id.players_summary)?.text = ""
        }
    }
}