package com.soniccomputing.golfscoretracker.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.soniccomputing.golfscoretracker.R
import com.soniccomputing.golfscoretracker.databinding.ChooseCourseBinding
import com.soniccomputing.golfscoretracker.ui.GolfCourseAdapter
import com.soniccomputing.golfscoretracker.viewmodel.TrackerViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class ChooseCourseFragment : Fragment(R.layout.choose_course) {
    private val trackerViewModel: TrackerViewModel by sharedViewModel()
    private lateinit var binding: ChooseCourseBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        ChooseCourseBinding.inflate(inflater, container, false).apply { binding = this }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = GolfCourseAdapter(layoutInflater) {
            trackerViewModel.setCourse(it)
            findNavController().popBackStack()
        }
        binding.golfCourses.apply {
            setAdapter(adapter)
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(
                DividerItemDecoration(
                    activity,
                    DividerItemDecoration.VERTICAL
                )
            )
        }
        adapter.submitList(trackerViewModel.courses)
    }
}