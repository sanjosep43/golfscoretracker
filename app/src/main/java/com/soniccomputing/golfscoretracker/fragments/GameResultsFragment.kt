package com.soniccomputing.golfscoretracker.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.soniccomputing.golfscoretracker.R
import com.soniccomputing.golfscoretracker.databinding.EndResultsBinding
import com.soniccomputing.golfscoretracker.viewmodel.TrackerViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class GameResultsFragment : Fragment(R.layout.end_results) {
    private val trackerViewModel: TrackerViewModel by sharedViewModel()
    private lateinit var binding: EndResultsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = EndResultsBinding.inflate(inflater, container, false).apply { binding = this }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}