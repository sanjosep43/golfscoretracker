package com.soniccomputing.golfscoretracker.fragments

import ZoomOutPageTransformer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.soniccomputing.golfscoretracker.R
import com.soniccomputing.golfscoretracker.viewmodel.TrackerViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class GamePlayPagerFragment : Fragment() {
    private lateinit var viewPager: ViewPager2
    private val trackerViewModel: TrackerViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.game_play, container, false)
        viewPager = view.findViewById(R.id.pager)

        val pagerAdapter = ScreenSlidePagerAdapter(requireActivity())
        viewPager.adapter = pagerAdapter
        viewPager.setPageTransformer(ZoomOutPageTransformer())

        trackerViewModel.startTrackerGame()

        return view
    }

    private inner class ScreenSlidePagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {
        override fun getItemCount(): Int = trackerViewModel.getNumberOfHoles()

        override fun createFragment(position: Int): Fragment {
            return EnterHoleResultFragment.getInstance(position)
        }
    }
}