package com.soniccomputing.golfscoretracker.factory

import com.soniccomputing.golfscoretracker.tracker.SkinsTracker
import com.soniccomputing.golfscoretracker.tracker.Tracker

const val SKINS = "Skins"

object TrackerFactory {
    fun create(name: String): Tracker? {
        if (name == SKINS) {
            return SkinsTracker()
        }
        return null
    }
}