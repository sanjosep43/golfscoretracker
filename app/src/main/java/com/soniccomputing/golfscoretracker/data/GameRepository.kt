package com.soniccomputing.golfscoretracker.data

import com.soniccomputing.golfscoretracker.factory.SKINS
import com.soniccomputing.golfscoretracker.model.GameModel

class GameRepository {
    val games = listOf(GameModel(name = SKINS))
}