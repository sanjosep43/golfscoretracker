package com.soniccomputing.golfscoretracker.data

import com.soniccomputing.golfscoretracker.model.GolfCourse
import com.soniccomputing.golfscoretracker.model.GolfHole

class TrackerRepository {
    var courses = listOf(
        GolfCourse(
            name = "Healdsburg Golf Course",
            holesMap = mapOf(
                1 to GolfHole(1, 9, 3, Pair(1, 9)),
                2 to GolfHole(2, 1, 5, Pair(2, 1)),
                3 to GolfHole(3, 8, 3, Pair(3, 8)),
                4 to GolfHole(4, 4, 4, Pair(4, 4)),
                5 to GolfHole(5, 5, 3, Pair(5, 5)),
                6 to GolfHole(6, 3, 4, Pair(6, 3)),
                7 to GolfHole(7, 6, 4, Pair(7, 6)),
                8 to GolfHole(8, 7, 4, Pair(8, 7)),
                9 to GolfHole(9, 2, 5, Pair(9, 2)),
            )
        ),
        GolfCourse(
            name = "Windsor",
            holesMap = mapOf(
                1 to GolfHole(1, 1, 3, Pair(1, 1)),
            )
        )
    )
}