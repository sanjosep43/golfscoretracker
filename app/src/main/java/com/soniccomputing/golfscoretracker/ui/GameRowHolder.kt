package com.soniccomputing.golfscoretracker.ui

import androidx.recyclerview.widget.RecyclerView
import com.soniccomputing.golfscoretracker.databinding.GameRowBinding
import com.soniccomputing.golfscoretracker.model.GameModel

class GameRowHolder(
    private val binding: GameRowBinding,
    private val setPlayersScreen: (GameModel) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(model: GameModel) {
        binding.apply {
            name.text = model.name
            name.setOnClickListener { setPlayersScreen(model) }
        }
    }
}