package com.soniccomputing.golfscoretracker.ui

import androidx.recyclerview.widget.RecyclerView
import com.soniccomputing.golfscoretracker.databinding.GolfCourseRowBinding
import com.soniccomputing.golfscoretracker.model.GolfCourse

class GolfCourseRowHolder(
    private val binding: GolfCourseRowBinding,
    val onClick: (GolfCourse) -> Unit
) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(model: GolfCourse) {
        binding.apply {
            name.text = model.name
            name.setOnClickListener { onClick(model) }
        }
    }
}