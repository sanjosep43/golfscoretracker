package com.soniccomputing.golfscoretracker.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.soniccomputing.golfscoretracker.databinding.TotalResultRowBinding
import com.soniccomputing.golfscoretracker.tracker.TotalPlayerScore

class PlayerResultAdapter(
    private val inflater: LayoutInflater
) : ListAdapter<TotalPlayerScore, TotalResultRowHolder>(TotalPlayerScoreDiff) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TotalResultRowHolder {
        return TotalResultRowHolder(TotalResultRowBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: TotalResultRowHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

object TotalPlayerScoreDiff : DiffUtil.ItemCallback<TotalPlayerScore>() {
    override fun areItemsTheSame(oldItem: TotalPlayerScore, newItem: TotalPlayerScore) =
        oldItem.player.id == newItem.player.id

    override fun areContentsTheSame(oldItem: TotalPlayerScore, newItem: TotalPlayerScore) =
        oldItem.player.name == newItem.player.name
}