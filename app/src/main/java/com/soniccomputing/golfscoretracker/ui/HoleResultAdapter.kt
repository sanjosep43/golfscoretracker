package com.soniccomputing.golfscoretracker.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.soniccomputing.golfscoretracker.databinding.HoleResultRowBinding
import com.soniccomputing.golfscoretracker.tracker.HoleResult

class HoleResultAdapter(
    private val inflater: LayoutInflater,
) :
    ListAdapter<HoleResult, HoleResultRowHolder>(HoleDiffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        HoleResultRowHolder(
            HoleResultRowBinding.inflate(inflater, parent, false),
        )

    override fun onBindViewHolder(holder: HoleResultRowHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

private object HoleDiffCallback : DiffUtil.ItemCallback<HoleResult>() {
    override fun areItemsTheSame(oldItem: HoleResult, newItem: HoleResult) =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: HoleResult, newItem: HoleResult) =
        oldItem.id == newItem.id
}