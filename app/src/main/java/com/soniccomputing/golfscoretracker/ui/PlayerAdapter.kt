package com.soniccomputing.golfscoretracker.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.soniccomputing.golfscoretracker.databinding.PlayerRowBinding
import com.soniccomputing.golfscoretracker.model.Player

class PlayerAdapter(
    private val inflater: LayoutInflater,
    private val deletePlayer: (Player) -> Unit
) : ListAdapter<Player, PlayerRowHolder>(PlayerDiffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerRowHolder {
        return PlayerRowHolder(PlayerRowBinding.inflate(inflater, parent, false), deletePlayer)
    }

    override fun onBindViewHolder(holder: PlayerRowHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

object PlayerDiffCallback : DiffUtil.ItemCallback<Player>() {
    override fun areItemsTheSame(oldItem: Player, newItem: Player) =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Player, newItem: Player) =
        oldItem.name == newItem.name
}