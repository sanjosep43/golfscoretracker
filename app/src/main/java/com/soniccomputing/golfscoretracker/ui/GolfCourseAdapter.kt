package com.soniccomputing.golfscoretracker.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.soniccomputing.golfscoretracker.databinding.GolfCourseRowBinding
import com.soniccomputing.golfscoretracker.model.GolfCourse

class GolfCourseAdapter(
    private val inflater: LayoutInflater,
    private val onClick: (GolfCourse) -> Unit
) :
    ListAdapter<GolfCourse, GolfCourseRowHolder>(DiffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        GolfCourseRowHolder(
            GolfCourseRowBinding.inflate(inflater, parent, false),
            onClick
        )

    override fun onBindViewHolder(holder: GolfCourseRowHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

private object DiffCallback : DiffUtil.ItemCallback<GolfCourse>() {
    override fun areItemsTheSame(oldItem: GolfCourse, newItem: GolfCourse) =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: GolfCourse, newItem: GolfCourse) =
        oldItem.id == newItem.id
}