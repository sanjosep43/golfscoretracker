package com.soniccomputing.golfscoretracker.ui

import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.RecyclerView
import com.soniccomputing.golfscoretracker.databinding.EnterPlayerScoreRowBinding
import com.soniccomputing.golfscoretracker.tracker.PlayerScore

class EnterPlayerScoreRowHolder(
    private val binding: EnterPlayerScoreRowBinding
): RecyclerView.ViewHolder(binding.root) {
    fun bind(model: PlayerScore) {
        binding.apply {
            playerName.text = model.player.name
            score.doAfterTextChanged {
                if(it?.isNotEmpty()!!) {
                    model.score = it.toString().toInt()
                }
            }
        }
    }
}