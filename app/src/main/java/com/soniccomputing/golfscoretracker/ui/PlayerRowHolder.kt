package com.soniccomputing.golfscoretracker.ui

import androidx.recyclerview.widget.RecyclerView
import com.soniccomputing.golfscoretracker.databinding.PlayerRowBinding
import com.soniccomputing.golfscoretracker.model.Player

class PlayerRowHolder(
    private val binding: PlayerRowBinding,
    private val deletePlayer: (Player) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(model: Player) {
        binding.apply {
            name.text = model.name
            deletePlayer.setOnClickListener { deletePlayer(model) }
        }
    }
}