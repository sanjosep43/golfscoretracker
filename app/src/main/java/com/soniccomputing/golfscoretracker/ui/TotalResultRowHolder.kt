package com.soniccomputing.golfscoretracker.ui

import androidx.recyclerview.widget.RecyclerView
import com.soniccomputing.golfscoretracker.R
import com.soniccomputing.golfscoretracker.databinding.TotalResultRowBinding
import com.soniccomputing.golfscoretracker.tracker.TotalPlayerScore

class TotalResultRowHolder(
    private val binding: TotalResultRowBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(model: TotalPlayerScore) {
        binding.apply {
            playerTotal.text = root.context.getString(
                R.string.total_player_result,
                model.player.name,
                model.totalWon
            )
        }
    }
}