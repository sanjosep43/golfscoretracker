package com.soniccomputing.golfscoretracker.ui

import androidx.recyclerview.widget.RecyclerView
import com.soniccomputing.golfscoretracker.R
import com.soniccomputing.golfscoretracker.databinding.HoleResultRowBinding
import com.soniccomputing.golfscoretracker.tracker.HoleResult

class HoleResultRowHolder(
    private val binding: HoleResultRowBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(model: HoleResult) {
        binding.apply {
            if (model.tie) {
                holeResult.text = root.context.getString(R.string.hole_tied, model.count)
            } else {
                val winner = model.winners[0]
                val text =
                    root.context.getString(
                        R.string.player_name_with_skin_count,
                        winner.name,
                        model.values[winner]!![0]
                    )
                holeResult.text = text
            }
        }
    }
}