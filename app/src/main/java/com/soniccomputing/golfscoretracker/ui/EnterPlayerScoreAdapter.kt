package com.soniccomputing.golfscoretracker.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.soniccomputing.golfscoretracker.databinding.EnterPlayerScoreRowBinding
import com.soniccomputing.golfscoretracker.tracker.PlayerScore

class EnterPlayerScoreAdapter(
    private val inflater: LayoutInflater
) : ListAdapter<PlayerScore, EnterPlayerScoreRowHolder>(PlayerDiffCallback2) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EnterPlayerScoreRowHolder {
        return EnterPlayerScoreRowHolder(EnterPlayerScoreRowBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: EnterPlayerScoreRowHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

object PlayerDiffCallback2 : DiffUtil.ItemCallback<PlayerScore>() {
    override fun areItemsTheSame(oldItem: PlayerScore, newItem: PlayerScore) =
        oldItem.player.id == newItem.player.id

    override fun areContentsTheSame(oldItem: PlayerScore, newItem: PlayerScore) =
        oldItem.player.name == newItem.player.name
}