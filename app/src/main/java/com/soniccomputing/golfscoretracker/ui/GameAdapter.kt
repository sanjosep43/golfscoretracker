package com.soniccomputing.golfscoretracker.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.soniccomputing.golfscoretracker.databinding.GameRowBinding
import com.soniccomputing.golfscoretracker.model.GameModel

class GameAdapter(
    private val inflater: LayoutInflater,
    private val setPlayersScreen: (GameModel) -> Unit
) :
    ListAdapter<GameModel, GameRowHolder>(GameDiffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameRowHolder {
        return GameRowHolder(GameRowBinding.inflate(inflater, parent, false), setPlayersScreen)
    }

    override fun onBindViewHolder(holder: GameRowHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

private object GameDiffCallback : DiffUtil.ItemCallback<GameModel>() {
    override fun areItemsTheSame(oldItem: GameModel, newItem: GameModel) =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: GameModel, newItem: GameModel) =
        oldItem.name == newItem.name
}