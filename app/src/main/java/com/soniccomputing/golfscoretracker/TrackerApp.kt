package com.soniccomputing.golfscoretracker

import android.app.Application
import com.soniccomputing.golfscoretracker.data.GameRepository
import com.soniccomputing.golfscoretracker.data.TrackerRepository
import com.soniccomputing.golfscoretracker.viewmodel.TrackerViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

class TrackerApp : Application() {
    private val koinModule = module {
        single { TrackerRepository() }
        single { GameRepository() }
        viewModel { TrackerViewModel(get(), get()) }
    }
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(org.koin.core.logger.Level.ERROR)
            androidContext(this@TrackerApp)
            modules(koinModule)
        }
    }
}