package com.soniccomputing.golfscoretracker.tracker

import androidx.lifecycle.MutableLiveData
import com.soniccomputing.golfscoretracker.model.GolfCourse
import com.soniccomputing.golfscoretracker.model.GolfHole
import com.soniccomputing.golfscoretracker.model.Player
import java.time.Instant
import java.util.*

data class PlayerHoles(
    val player: Player,
    var count: Int = 0,
    var holesWon: MutableList<GolfHole> = mutableListOf<GolfHole>(),
    var holesTied: MutableList<GolfHole> = mutableListOf<GolfHole>(),
)

data class PlayerScore(
    val player: Player,
    var score: Int = 0,
    var normalizedScore: Int = 0,
    val id: String = UUID.randomUUID().toString(),
    val createdOn: Instant = Instant.now(),
)

data class TotalPlayerScore(
    val player: Player,
    var totalWon: Int = 0,
    var totalTied: Int = 0,
    var totalLost: Int = 0,
    val id: String = UUID.randomUUID().toString(),
    val createdOn: Instant = Instant.now(),
)

data class HoleResult(
    val winners: List<Player>,
    val winningScore: Int,
    val tie: Boolean,
    val number: Int,
    val values: Map<Player, List<Int>> = mapOf(),
    val count: Int,
    val id: String = UUID.randomUUID().toString(),
    val createdOn: Instant = Instant.now(),
)

interface Tracker {
    val _holeResult: MutableLiveData<HoleResult>
    fun start(players: List<Player>, golfCourse: GolfCourse)
    fun addScoresForHole(playerScores: List<PlayerScore>, hole: Int)
    fun getPlayerHoles(): List<PlayerHoles>
    fun getHoleResults(): List<HoleResult>
    fun end()
}