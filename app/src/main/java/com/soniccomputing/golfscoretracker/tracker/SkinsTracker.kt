package com.soniccomputing.golfscoretracker.tracker

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.soniccomputing.golfscoretracker.model.GolfCourse
import com.soniccomputing.golfscoretracker.model.GolfHole
import com.soniccomputing.golfscoretracker.model.Player
import com.soniccomputing.golfscoretracker.model.ScoreCard
import java.util.*


class SkinsTracker : Tracker {
    lateinit var scoreCard: ScoreCard

    override val _holeResult = MutableLiveData<HoleResult>()
    val holeResult: LiveData<HoleResult>
        get() = _holeResult

    private val _skinsWonCount = MutableLiveData<Int>()
    val skinsWonCount: LiveData<Int>
        get() = _skinsWonCount

    private val _gameOver = MutableLiveData<Boolean>()
    val gameOver: LiveData<Boolean>
        get() = _gameOver

    var skinsByPlayerMap = mutableMapOf<Player, PlayerHoles>()
    private var _holeResults = mutableListOf<HoleResult>()

    private var internalSkinCount = 0
    private var tiedHoles = mutableListOf<GolfHole>()

    /*
    will find the normalizedHoles for each player.
    1. sort the holes by difficulty
    2. get the players handicap and subtract a point for each hole
    3.
     */
    override fun start(players: List<Player>, golfCourse: GolfCourse) {
        scoreCard = ScoreCard(
            players = players,
            dateTime = Date(),
            golfCourse = golfCourse
        )
        val holes = golfCourse.holesMap.map { it.value }
        // sort the holes by difficultly in the golf course
        val sortedHoles = holes.sortedBy<GolfHole, Int> { it.numberAndHandicap.second }
        // copy holes to players normalized holes
        players.forEach { player ->
            sortedHoles.forEach { player.normalizedHolesMap[it.number] = it.par }
        }
        // add handicap to each hole in order
        players.forEach { player ->
            var playerHandicap = player.handicap
            while (playerHandicap > 0) {
                sortedHoles.forEach {
                    if (playerHandicap > 0) {
                        player.normalizedHolesMap[it.number] =
                            player.normalizedHolesMap[it.number]!! + 1
                        playerHandicap -= 1
                    }
                }
            }
        }
        players.forEach {
            skinsByPlayerMap[it] = PlayerHoles(player = it)
        }
        _gameOver.value = false
        _holeResults.clear()
        tiedHoles.clear()
        internalSkinCount = 0
    }

    override fun addScoresForHole(playerScores: List<PlayerScore>, hole: Int) {
        // who won the hole: subtract actual score from normalized score...negative number for better
        playerScores.forEach {
            it.normalizedScore = it.score - it.player.normalizedHolesMap[hole]!!
        }
        // sort players score ascending
        val sortedPlayerScores = playerScores.sortedBy { it.normalizedScore }
        val winningScore = sortedPlayerScores[0].normalizedScore
        // find winners
        val winners =
            sortedPlayerScores.takeWhile { it.normalizedScore == winningScore }.map { it.player }
        //find number of skins won
        _skinsWonCount.value = if (isHoleTied(winners)) {
            scoreCard.players.forEach {
                skinsByPlayerMap[it]!!.holesTied.add(scoreCard.golfCourse.holesMap[hole]!!)
            }
            tiedHoles.add(scoreCard.golfCourse.holesMap[hole]!!)
            internalSkinCount += 1
            0
        } else {
            val skinWinCount = internalSkinCount + 1
            skinsByPlayerMap[winners[0]]!!.holesWon.addAll(tiedHoles)
            skinsByPlayerMap[winners[0]]!!.holesWon.add(scoreCard.golfCourse.holesMap[hole]!!)
            skinsByPlayerMap[winners[0]]!!.count += skinWinCount
            skinWinCount
        }
        if (!isHoleTied(winners)) {
            internalSkinCount = 0
            tiedHoles.clear()
        }
        //tally result
        val values = mutableMapOf<Player, List<Int>>()
        skinsByPlayerMap.forEach {
            values[it.key] = listOf(
                skinsByPlayerMap[it.key]!!.count
            )
        }
        val holeResult =
            HoleResult(
                winners,
                sortedPlayerScores[0].score,
                isHoleTied(winners),
                hole,
                values,
                internalSkinCount
            )
        _holeResults.add(holeResult)
        _holeResult.value = holeResult
    }

    private fun isHoleTied(winners: List<Player>) = winners.size != 1

    override fun end() {
        _gameOver.value = true
    }

    override fun getPlayerHoles(): List<PlayerHoles> {
        return listOf()
    }

    override fun getHoleResults(): List<HoleResult> {
        return _holeResults.toList()
    }
}