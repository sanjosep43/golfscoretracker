package com.soniccomputing.golfscoretracker.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.soniccomputing.golfscoretracker.data.GameRepository
import com.soniccomputing.golfscoretracker.data.TrackerRepository
import com.soniccomputing.golfscoretracker.factory.TrackerFactory
import com.soniccomputing.golfscoretracker.model.GameModel
import com.soniccomputing.golfscoretracker.model.GolfCourse
import com.soniccomputing.golfscoretracker.model.GolfHole
import com.soniccomputing.golfscoretracker.model.Player
import com.soniccomputing.golfscoretracker.tracker.HoleResult
import com.soniccomputing.golfscoretracker.tracker.PlayerScore
import com.soniccomputing.golfscoretracker.tracker.Tracker

class TrackerViewModel(trackerRepo: TrackerRepository, gameRepo: GameRepository) : ViewModel() {
    lateinit var currentCourse: GolfCourse
    lateinit var currentGame: GameModel
    var currentHoleNumber: Int = 1
    var players = mutableListOf<Player>()
    var courses = trackerRepo.courses
    var games = gameRepo.games
    private lateinit var tracker: Tracker
    private val holeResults = mutableListOf<HoleResult>()
    private val _holeResult = MutableLiveData<HoleResult>()
    val holeResult: LiveData<HoleResult>
        get() = _holeResult

    fun isCurrentGameInitialized() = this::currentGame.isInitialized
    fun isCurrentCourseInitialized() = this::currentCourse.isInitialized

    fun setCourse(course: GolfCourse) {
        currentCourse = course
    }

    fun setGame(game: GameModel) {
        currentGame = game
    }

    fun getCurrentHoleInfo(): GolfHole {
        return currentCourse.holesMap[currentHoleNumber] ?: error("not valid hole number!")
    }

    fun goToNextHole(): Boolean {
        if (currentCourse.holesMap.size == currentHoleNumber) {
            return false
        }
        currentHoleNumber += 1
        return true
    }

    fun goToPrevHole(): Boolean {
        if (1 == currentHoleNumber) {
            return false
        }
        currentHoleNumber -= 1
        return true
    }

    fun getHoleInfo(hole: Int): GolfHole {
        return currentCourse.holesMap[hole] ?: error("not valid hole number!")
    }

    fun getNumberOfHoles(): Int {
        return currentCourse.holesMap.size
    }

    fun startTrackerGame() {
        tracker = TrackerFactory.create(currentGame.name) ?: error("Invalid Game")
        tracker.start(players, currentCourse)
        tracker._holeResult.observeForever {
            holeResults.add(it)
            _holeResult.value = it
        }
    }

    fun enterHoleResult(playerScores: List<PlayerScore>, hole: Int) {
        tracker.addScoresForHole(playerScores, hole)
    }

    fun endTrackerGame() {
        tracker.end()
    }

    fun getWinningCountForPlayer(player: Player): Int {
//        return holeResults.sumOf {
//            if(!it.tie && it.winners[0].id == player.id) {
//                return it.count
//            }
//            return 0
//        }
        val list: List<Int> = holeResults.map { holeResult ->
            if (!holeResult.tie && holeResult.winners[0].id == player.id) {
                return holeResult.count
            }
            return 0
        }
        return list.sum()
    }

    fun getTiedHolesCount() {
        holeResults.last().count
    }
}