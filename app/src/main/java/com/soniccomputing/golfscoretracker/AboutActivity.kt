package com.soniccomputing.golfscoretracker

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.soniccomputing.golfscoretracker.databinding.ActivityAboutBinding

class AboutActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityAboutBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.toolbar.title = getString(R.string.app_name)
        binding.about.loadUrl("file:///android_asset/about.html")
    }
}