package com.soniccomputing.golfscoretracker.model

import java.time.Instant
import java.util.*

data class GolfCourse(
    val name: String,
    val holesMap: Map<Int, GolfHole>,
    val id: String = UUID.randomUUID().toString(),
    val createdOn: Instant = Instant.now()
)
