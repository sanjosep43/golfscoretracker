package com.soniccomputing.golfscoretracker.model

import java.time.Instant
import java.util.*

data class ScoreCard(
    val golfCourse: GolfCourse,
    val dateTime: Date,
    val players: List<Player>,
    val scores: List<GolfHole> = listOf(),
    val scoresMap: Map<Int, GolfHole> = mapOf(),
    val id: String = UUID.randomUUID().toString(),
    val createdOn: Instant = Instant.now()
)
