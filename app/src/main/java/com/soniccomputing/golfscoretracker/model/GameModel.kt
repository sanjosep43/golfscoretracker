package com.soniccomputing.golfscoretracker.model

import java.util.*

data class GameModel(
    val name: String,
    val id: String = UUID.randomUUID().toString(),
)