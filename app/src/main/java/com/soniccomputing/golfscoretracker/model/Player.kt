package com.soniccomputing.golfscoretracker.model

import java.time.Instant
import java.util.*

data class Player(
    val name: String,
    val handicap: Int,
    var normalizedHoles: List<Int> = listOf(),
    var normalizedHolesMap: MutableMap<Int, Int> = mutableMapOf(),
    val id: String = UUID.randomUUID().toString(),
    val createdOn: Instant = Instant.now()
)
