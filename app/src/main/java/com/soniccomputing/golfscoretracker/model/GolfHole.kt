package com.soniccomputing.golfscoretracker.model

import java.time.Instant
import java.util.*

data class GolfHole(
    val number: Int,
    val handicap: Int,
    var par: Int,
    val numberAndHandicap: Pair<Int, Int>,
    var winners: List<Player> = listOf(),
    val id: String = UUID.randomUUID().toString(),
    val createdOn: Instant = Instant.now()

)
