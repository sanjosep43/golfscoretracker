package com.soniccomputing.golfscoretracker.tracker

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.soniccomputing.golfscoretracker.model.GolfCourse
import com.soniccomputing.golfscoretracker.model.GolfHole
import com.soniccomputing.golfscoretracker.model.Player
import io.mockk.*
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test

class SkinsTrackerTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private fun smallGolfCourse(): Map<Int, GolfHole> {
       return mapOf(
           Pair(1, GolfHole(1, 3, 3, Pair(1, 3))),
           Pair(2, GolfHole(2, 1, 4, Pair(2, 1))),
           Pair(3, GolfHole(3, 2, 5, Pair(3, 2))),
       )
    }

    @Test
    fun `start 3 holes, 2 players, 0 handicap`() {
        val skinsTracker = SkinsTracker()
        val players = listOf(
            Player("player1", 0),
            Player("player2", 0),
        )

        val golfHolesMap = smallGolfCourse()
        val golfCourse = GolfCourse("G", golfHolesMap)

        skinsTracker.start(players, golfCourse)

        assertEquals(skinsTracker.scoreCard.players[0].normalizedHolesMap[1], 3)
        assertEquals(skinsTracker.scoreCard.players[0].normalizedHolesMap[2], 4)
        assertEquals(skinsTracker.scoreCard.players[0].normalizedHolesMap[3], 5)

        assertEquals(skinsTracker.scoreCard.players[1].normalizedHolesMap[1], 3)
        assertEquals(skinsTracker.scoreCard.players[1].normalizedHolesMap[2], 4)
        assertEquals(skinsTracker.scoreCard.players[1].normalizedHolesMap[3], 5)
    }

    @Test
    fun `start 3 holes, 2 players, equal handicap`() {
        val skinsTracker = SkinsTracker()
        val players = listOf(
            Player("player1", 9),
            Player("player2", 9),
        )

        val golfHolesMap = smallGolfCourse()
        val golfCourse = GolfCourse("G", golfHolesMap)

        skinsTracker.start(players, golfCourse)

        assertEquals(skinsTracker.scoreCard.players[0].normalizedHolesMap[1], 6)
        assertEquals(skinsTracker.scoreCard.players[0].normalizedHolesMap[2], 7)
        assertEquals(skinsTracker.scoreCard.players[0].normalizedHolesMap[3], 8)

        assertEquals(skinsTracker.scoreCard.players[1].normalizedHolesMap[1], 6)
        assertEquals(skinsTracker.scoreCard.players[1].normalizedHolesMap[2], 7)
        assertEquals(skinsTracker.scoreCard.players[1].normalizedHolesMap[3], 8)
    }

    @Test
    fun `start 3 holes, 2 players, 1 player small handicap`() {
        val skinsTracker = SkinsTracker()
        val players = listOf(
            Player("player1", 0),
            Player("player2", 1),
        )

        val golfHolesMap = smallGolfCourse()
        val golfCourse = GolfCourse("G", golfHolesMap)

        skinsTracker.start(players, golfCourse)

        assertEquals(skinsTracker.scoreCard.players[0].normalizedHolesMap[1], 3)
        assertEquals(skinsTracker.scoreCard.players[0].normalizedHolesMap[2], 4)
        assertEquals(skinsTracker.scoreCard.players[0].normalizedHolesMap[3], 5)

        assertEquals(skinsTracker.scoreCard.players[1].normalizedHolesMap[1], 3)
        assertEquals(skinsTracker.scoreCard.players[1].normalizedHolesMap[2], 5)
        assertEquals(skinsTracker.scoreCard.players[1].normalizedHolesMap[3], 5)
    }

    @Test
    fun `start 3 holes, 2 players, 1 player 2 handicap`() {
        val skinsTracker = SkinsTracker()
        val players = listOf(
            Player("player1", 0),
            Player("player2", 2),
        )

        val golfHolesMap = smallGolfCourse()
        val golfCourse = GolfCourse("G", golfHolesMap)

        skinsTracker.start(players, golfCourse)

        assertEquals(skinsTracker.scoreCard.players[0].normalizedHolesMap[1], 3)
        assertEquals(skinsTracker.scoreCard.players[0].normalizedHolesMap[2], 4)
        assertEquals(skinsTracker.scoreCard.players[0].normalizedHolesMap[3], 5)

        assertEquals(skinsTracker.scoreCard.players[1].normalizedHolesMap[1], 3)
        assertEquals(skinsTracker.scoreCard.players[1].normalizedHolesMap[2], 5)
        assertEquals(skinsTracker.scoreCard.players[1].normalizedHolesMap[3], 6)
    }

    @Test
    fun `start 3 holes, 2 players, 1 player big handicap`() {
        val skinsTracker = SkinsTracker()
        val players = listOf(
            Player("player1", 0),
            Player("player2", 9),
        )

        val golfHolesMap = smallGolfCourse()
        val golfCourse = GolfCourse("G", golfHolesMap)

        skinsTracker.start(players, golfCourse)

        assertEquals(skinsTracker.scoreCard.players[0].normalizedHolesMap[1], 3)
        assertEquals(skinsTracker.scoreCard.players[0].normalizedHolesMap[2], 4)
        assertEquals(skinsTracker.scoreCard.players[0].normalizedHolesMap[3], 5)

        assertEquals(skinsTracker.scoreCard.players[1].normalizedHolesMap[1], 6)
        assertEquals(skinsTracker.scoreCard.players[1].normalizedHolesMap[2], 7)
        assertEquals(skinsTracker.scoreCard.players[1].normalizedHolesMap[3], 8)
    }

    @Test
    fun `addScoresForHole 2 players, player 1 winner`() {
        val skinsTracker = SkinsTracker()
        val players = listOf(
            Player("player1", 0),
            Player("player2", 9),
        )

        val golfHolesMap = smallGolfCourse()
        val golfCourse = GolfCourse("G", golfHolesMap)

        skinsTracker.start(players, golfCourse)
        val playerScore1 = PlayerScore(players[0], 5) // 2 over
        val playerScore2 = PlayerScore(players[1], 9) // 3 over

        val observer = mockk<Observer<HoleResult>> { every { onChanged(any()) } just Runs }
        val slot = slot<HoleResult>()
        val list = arrayListOf<HoleResult>()
        skinsTracker.holeResult.observeForever(observer)
        every { observer.onChanged(capture(slot)) } answers {
            list.add(slot.captured)
        }

        skinsTracker.addScoresForHole(listOf(playerScore1, playerScore2), 1)

        assertEquals(list[0].winners[0].name, "player1")
        assertEquals(list[0].winningScore, 5)
        assertEquals(list[0].tie, false)
    }

    @Test
    fun `addScoresForHole 2 players, player 2 winner`() {
        val skinsTracker = SkinsTracker()
        val players = listOf(
            Player("player1", 0),
            Player("player2", 9),
        )

        val golfHolesMap = smallGolfCourse()
        val golfCourse = GolfCourse("G", golfHolesMap)

        skinsTracker.start(players, golfCourse)
        val playerScore1 = PlayerScore(players[0], 5) // 2 over
        val playerScore2 = PlayerScore(players[1], 6) // 3 over

        val observer = mockk<Observer<HoleResult>> { every { onChanged(any()) } just Runs }
        val slot = slot<HoleResult>()
        val list = arrayListOf<HoleResult>()
        skinsTracker.holeResult.observeForever(observer)
        every { observer.onChanged(capture(slot)) } answers {
            list.add(slot.captured)
        }

        skinsTracker.addScoresForHole(listOf(playerScore1, playerScore2), 1)

        assertEquals(list[0].winners[0].name, "player2")
        assertEquals(list[0].winningScore, 6)
        assertEquals(list[0].tie, false)
    }

    @Test
    fun `addScoresForHole 2 players, tie`() {
        val skinsTracker = SkinsTracker()
        val players = listOf(
            Player("player1", 0),
            Player("player2", 9),
        )

        val golfHolesMap = smallGolfCourse()
        val golfCourse = GolfCourse("G", golfHolesMap)

        skinsTracker.start(players, golfCourse)
        val playerScore1 = PlayerScore(players[0], 3) // par
        val playerScore2 = PlayerScore(players[1], 6) // 3 over par

        val observer = mockk<Observer<HoleResult>> { every { onChanged(any()) } just Runs }
        val slot = slot<HoleResult>()
        val list = arrayListOf<HoleResult>()
        skinsTracker.holeResult.observeForever(observer)
        every { observer.onChanged(capture(slot)) } answers {
            list.add(slot.captured)
        }

        skinsTracker.addScoresForHole(listOf(playerScore1, playerScore2), 1)

        assertEquals(list[0].winners[0].name, "player1")
        assertEquals(list[0].winners[1].name, "player2")
        assertEquals(list[0].winningScore, 3)
        assertEquals(list[0].tie, true)
    }

    @Test
    fun `addScoresForHole 4 players, player 3 winner`() {
        val skinsTracker = SkinsTracker()
        val players = listOf(
            Player("player1", 0),
            Player("player2", 9),
            Player("player3", 7),
            Player("player4", 3),
        )

        val golfHolesMap = smallGolfCourse()
        val golfCourse = GolfCourse("G", golfHolesMap)

        skinsTracker.start(players, golfCourse)
        val playerScore1 = PlayerScore(players[0], 6) // 2 over
        val playerScore2 = PlayerScore(players[1], 8) // 4 over
        val playerScore3 = PlayerScore(players[2], 5) // 1 over
        val playerScore4 = PlayerScore(players[3], 8) // 4 over

        val observer = mockk<Observer<HoleResult>> { every { onChanged(any()) } just Runs }
        val slot = slot<HoleResult>()
        val list = arrayListOf<HoleResult>()
        skinsTracker.holeResult.observeForever(observer)
        every { observer.onChanged(capture(slot)) } answers {
            list.add(slot.captured)
        }

        skinsTracker.addScoresForHole(
            listOf(
                playerScore1,
                playerScore2,
                playerScore3,
                playerScore4
            ), 2
        )

        assertEquals(list[0].winners[0].name, "player3")
        assertEquals(list[0].winningScore, 5)
        assertEquals(list[0].tie, false)
    }

    @Test
    fun `addScoresForHole 4 players, 2 players tie`() {
        val skinsTracker = SkinsTracker()
        val players = listOf(
            Player("player1", 0),
            Player("player2", 3),
            Player("player3", 0),
            Player("player4", 0),
        )

        val golfHolesMap = smallGolfCourse()
        val golfCourse = GolfCourse("G", golfHolesMap)

        skinsTracker.start(players, golfCourse)
        val playerScore1 = PlayerScore(players[0], 4) // 1 over
        val playerScore2 = PlayerScore(players[1], 5) // 2 over
        val playerScore3 = PlayerScore(players[2], 5) // 2 over
        val playerScore4 = PlayerScore(players[3], 6) // 3 over

        val observer = mockk<Observer<HoleResult>> { every { onChanged(any()) } just Runs }
        val slot = slot<HoleResult>()
        val list = arrayListOf<HoleResult>()
        skinsTracker.holeResult.observeForever(observer)
        every { observer.onChanged(capture(slot)) } answers {
            list.add(slot.captured)
        }

        skinsTracker.addScoresForHole(
            listOf(
                playerScore1,
                playerScore2,
                playerScore3,
                playerScore4
            ), 1
        )

        assertEquals(list[0].winners[0].name, "player1")
        assertEquals(list[0].winners[1].name, "player2")
        assertEquals(list[0].winningScore, 4)
        assertEquals(list[0].tie, true)
    }

    @Test
    fun `addScoresForHole 4 players, 2 players tie, next hole 1 player wins 2 skins`(){
        val skinsTracker = SkinsTracker()
        val players = listOf(
            Player("player1", 0),
            Player("player2", 3),
            Player("player3", 3),
            Player("player4", 3),
        )

        val golfHolesMap = smallGolfCourse()
        val golfCourse = GolfCourse("G", golfHolesMap)

        skinsTracker.start(players, golfCourse)
        val playerScore1 = PlayerScore(players[0], 4) // 1 over
        val playerScore2 = PlayerScore(players[1], 4) // 1 over
        val playerScore3 = PlayerScore(players[2], 4) // 1 over
        val playerScore4 = PlayerScore(players[3], 6) // 2 over

        val observer = mockk<Observer<HoleResult>> { every { onChanged(any()) } just Runs }
        val slot = slot<HoleResult>()
        val list = arrayListOf<HoleResult>()
        skinsTracker.holeResult.observeForever(observer)
        every { observer.onChanged(capture(slot)) } answers {
            list.add(slot.captured)
        }
        val countObserver = mockk<Observer<Int>> { every { onChanged(any()) } just Runs }
        val countSlot = slot<Int>()
        val countList = arrayListOf<Int>()
        skinsTracker.skinsWonCount.observeForever(countObserver)
        every { countObserver.onChanged(capture(countSlot)) } answers {
            countList.add(countSlot.captured)
        }

        skinsTracker.addScoresForHole(
            listOf(
                playerScore1,
                playerScore2,
                playerScore3,
                playerScore4
            ), 1
        )

        assertEquals(list[0].winners[0].name, "player2")
        assertEquals(list[0].winners[1].name, "player3")
        assertEquals(list[0].winningScore, 4)
        assertEquals(list[0].tie, true)
        assertEquals(countList[0], 0)

        val playerScore1b = PlayerScore(players[0], 4) // par
        val playerScore2b = PlayerScore(players[1], 6) // 2 over
        val playerScore3b = PlayerScore(players[2], 6) // 2 over
        val playerScore4b = PlayerScore(players[3], 6) // 2 over

        skinsTracker.addScoresForHole(
            listOf(
                playerScore1b,
                playerScore2b,
                playerScore3b,
                playerScore4b
            ), 2
        )

        assertEquals(list[1].winners[0].name, "player1")
        assertEquals(list[1].winningScore, 4)
        assertEquals(list[1].tie, false)
        assertEquals(2, countList[1])
    }

    @Test
    fun `addScoresForHole 4 players, 2 players tie, next hole tie, then player 3 wins 3 skins`(){
        val skinsTracker = SkinsTracker()
        val players = listOf(
            Player("player1", 0),
            Player("player2", 3),
            Player("player3", 3),
            Player("player4", 3),
        )

        val golfHolesMap = smallGolfCourse()
        val golfCourse = GolfCourse("G", golfHolesMap)

        skinsTracker.start(players, golfCourse)
        val playerScore1 = PlayerScore(players[0], 4) // 1 over
        val playerScore2 = PlayerScore(players[1], 4) // 1 over
        val playerScore3 = PlayerScore(players[2], 4) // 1 over
        val playerScore4 = PlayerScore(players[3], 6) // 2 over

        val observer = mockk<Observer<HoleResult>> { every { onChanged(any()) } just Runs }
        val slot = slot<HoleResult>()
        val list = arrayListOf<HoleResult>()
        skinsTracker.holeResult.observeForever(observer)
        every { observer.onChanged(capture(slot)) } answers {
            list.add(slot.captured)
        }
        val countObserver = mockk<Observer<Int>> { every { onChanged(any()) } just Runs }
        val countSlot = slot<Int>()
        val countList = arrayListOf<Int>()
        skinsTracker.skinsWonCount.observeForever(countObserver)
        every { countObserver.onChanged(capture(countSlot)) } answers {
            countList.add(countSlot.captured)
        }

        skinsTracker.addScoresForHole(
            listOf(
                playerScore1,
                playerScore2,
                playerScore3,
                playerScore4
            ), 1
        )

        assertEquals(list[0].winners[0].name, "player2")
        assertEquals(list[0].winners[1].name, "player3")
        assertEquals(list[0].winningScore, 4)
        assertEquals(list[0].tie, true)
        assertEquals(countList[0], 0)

        val playerScore1b = PlayerScore(players[0], 4) // par
        val playerScore2b = PlayerScore(players[1], 5) // 1 over
        val playerScore3b = PlayerScore(players[2], 6) // 2 over
        val playerScore4b = PlayerScore(players[3], 6) // 2 over

        skinsTracker.addScoresForHole(
            listOf(
                playerScore1b,
                playerScore2b,
                playerScore3b,
                playerScore4b
            ), 2
        )

        assertEquals(list[1].winners[0].name, "player1")
        assertEquals(list[1].winners[1].name, "player2")
        assertEquals(list[1].winningScore, 4)
        assertEquals(list[1].tie, true)
        assertEquals(0, countList[1])

        val playerScore1c = PlayerScore(players[0], 7) // par
        val playerScore2c = PlayerScore(players[1], 8) // 1 over
        val playerScore3c = PlayerScore(players[2], 5) // 2 over
        val playerScore4c = PlayerScore(players[3], 7) // 2 over

        skinsTracker.addScoresForHole(
            listOf(
                playerScore1c,
                playerScore2c,
                playerScore3c,
                playerScore4c
            ), 3
        )

        assertEquals(list[2].winners[0].name, "player3")
        assertEquals(list[2].winningScore, 5)
        assertEquals(list[2].tie, false)
        assertEquals(3, countList[2])
    }

    @Test
    fun `addScoresForHole 4 players, 2 players tie, next hole winner takes 2 skins, then 1 player wins 1 skin`() {
        val skinsTracker = SkinsTracker()
        val players = listOf(
            Player("player1", 0),
            Player("player2", 3),
            Player("player3", 3),
            Player("player4", 3),
        )

        val golfHolesMap = smallGolfCourse()
        val golfCourse = GolfCourse("G", golfHolesMap)

        skinsTracker.start(players, golfCourse)
        val playerScore1 = PlayerScore(players[0], 4) // 1 over
        val playerScore2 = PlayerScore(players[1], 4) // 1 over
        val playerScore3 = PlayerScore(players[2], 4) // 1 over
        val playerScore4 = PlayerScore(players[3], 6) // 2 over

        val observer = mockk<Observer<HoleResult>> { every { onChanged(any()) } just Runs }
        val slot = slot<HoleResult>()
        val list = arrayListOf<HoleResult>()
        skinsTracker.holeResult.observeForever(observer)
        every { observer.onChanged(capture(slot)) } answers {
            list.add(slot.captured)
        }
        val countObserver = mockk<Observer<Int>> { every { onChanged(any()) } just Runs }
        val countSlot = slot<Int>()
        val countList = arrayListOf<Int>()
        skinsTracker.skinsWonCount.observeForever(countObserver)
        every { countObserver.onChanged(capture(countSlot)) } answers {
            countList.add(countSlot.captured)
        }

        skinsTracker.addScoresForHole(
            listOf(
                playerScore1,
                playerScore2,
                playerScore3,
                playerScore4
            ), 1
        )

        assertEquals(list[0].winners[0].name, "player2")
        assertEquals(list[0].winners[1].name, "player3")
        assertEquals(list[0].winningScore, 4)
        assertEquals(list[0].tie, true)
        assertEquals(countList[0], 0)

        val playerScore1b = PlayerScore(players[0], 4) // par
        val playerScore2b = PlayerScore(players[1], 6) // 2 over
        val playerScore3b = PlayerScore(players[2], 6) // 2 over
        val playerScore4b = PlayerScore(players[3], 6) // 2 over

        skinsTracker.addScoresForHole(
            listOf(
                playerScore1b,
                playerScore2b,
                playerScore3b,
                playerScore4b
            ), 2
        )

        assertEquals(list[1].winners[0].name, "player1")
        assertEquals(list[1].winningScore, 4)
        assertEquals(list[1].tie, false)
        assertEquals(2, countList[1])

        val playerScore1c = PlayerScore(players[0], 6) // par
        val playerScore2c = PlayerScore(players[1], 8) // 1 over
        val playerScore3c = PlayerScore(players[2], 5) // 2 over
        val playerScore4c = PlayerScore(players[3], 7) // 2 over

        skinsTracker.addScoresForHole(
            listOf(
                playerScore1c,
                playerScore2c,
                playerScore3c,
                playerScore4c
            ), 3
        )

        assertEquals(list[2].winners[0].name, "player3")
        assertEquals(list[2].winningScore, 5)
        assertEquals(list[2].tie, false)
        assertEquals(1, countList[2])
    }

    @Test
    fun `end`() {
        val skinsTracker = SkinsTracker()
        val players = listOf(
            Player("player1", 0),
            Player("player2", 3),
            Player("player3", 0),
            Player("player4", 0),
        )

        val golfHolesMap = smallGolfCourse()
        val golfCourse = GolfCourse("G", golfHolesMap)

        skinsTracker.start(players, golfCourse)

        val observer = mockk<Observer<Boolean>> { every { onChanged(any()) } just Runs }
        val slot = slot<Boolean>()
        val list = arrayListOf<Boolean>()
        skinsTracker.gameOver.observeForever(observer)
        every { observer.onChanged(capture(slot)) } answers {
            list.add(slot.captured)
        }

        skinsTracker.end()

        assertEquals(list[0], true)
    }

    @Test
    fun `check skin totals by player`() {
        val skinsTracker = SkinsTracker()
        val players = listOf(
            Player("player1", 0),
            Player("player2", 3),
            Player("player3", 3),
            Player("player4", 3),
        )

        val golfHolesMap = smallGolfCourse()
        val golfCourse = GolfCourse("G", golfHolesMap)

        skinsTracker.start(players, golfCourse)
        val playerScore1 = PlayerScore(players[0], 4) // 1 over
        val playerScore2 = PlayerScore(players[1], 4) // 1 over
        val playerScore3 = PlayerScore(players[2], 4) // 1 over
        val playerScore4 = PlayerScore(players[3], 6) // 2 over

        skinsTracker.addScoresForHole(
            listOf(
                playerScore1,
                playerScore2,
                playerScore3,
                playerScore4
            ), 1
        )

        val playerScore1b = PlayerScore(players[0], 4) // par
        val playerScore2b = PlayerScore(players[1], 6) // 2 over
        val playerScore3b = PlayerScore(players[2], 6) // 2 over
        val playerScore4b = PlayerScore(players[3], 6) // 2 over

        skinsTracker.addScoresForHole(
            listOf(
                playerScore1b,
                playerScore2b,
                playerScore3b,
                playerScore4b
            ), 2
        )

        val playerScore1c = PlayerScore(players[0], 6) // par
        val playerScore2c = PlayerScore(players[1], 8) // 1 over
        val playerScore3c = PlayerScore(players[2], 5) // 2 over
        val playerScore4c = PlayerScore(players[3], 7) // 2 over

        skinsTracker.addScoresForHole(
            listOf(
                playerScore1c,
                playerScore2c,
                playerScore3c,
                playerScore4c
            ), 3
        )

        assertEquals(2, skinsTracker.skinsByPlayerMap[players[0]]!!.count)
        assertEquals(1, skinsTracker.skinsByPlayerMap[players[0]]!!.holesTied.size)
        assertEquals(2, skinsTracker.skinsByPlayerMap[players[0]]!!.holesWon.size)

        assertEquals(0, skinsTracker.skinsByPlayerMap[players[1]]!!.count)
        assertEquals(1, skinsTracker.skinsByPlayerMap[players[1]]!!.holesTied.size)
        assertEquals(0, skinsTracker.skinsByPlayerMap[players[1]]!!.holesWon.size)

        assertEquals(1, skinsTracker.skinsByPlayerMap[players[2]]!!.count)
        assertEquals(1, skinsTracker.skinsByPlayerMap[players[2]]!!.holesTied.size)
        assertEquals(1, skinsTracker.skinsByPlayerMap[players[2]]!!.holesWon.size)

        assertEquals(0, skinsTracker.skinsByPlayerMap[players[3]]!!.count)
        assertEquals(1, skinsTracker.skinsByPlayerMap[players[3]]!!.holesTied.size)
        assertEquals(0, skinsTracker.skinsByPlayerMap[players[3]]!!.holesWon.size)
    }

    @Test
    fun `check hole results`() {
        val skinsTracker = SkinsTracker()
        val players = listOf(
            Player("player1", 0),
            Player("player2", 3),
            Player("player3", 3),
            Player("player4", 3),
        )

        val golfHolesMap = smallGolfCourse()
        val golfCourse = GolfCourse("G", golfHolesMap)

        skinsTracker.start(players, golfCourse)
        val playerScore1 = PlayerScore(players[0], 4) // 1 over
        val playerScore2 = PlayerScore(players[1], 4) // 1 over
        val playerScore3 = PlayerScore(players[2], 4) // 1 over
        val playerScore4 = PlayerScore(players[3], 6) // 2 over

        skinsTracker.addScoresForHole(
            listOf(
                playerScore1,
                playerScore2,
                playerScore3,
                playerScore4
            ), 1
        )

        val playerScore1b = PlayerScore(players[0], 4) // par
        val playerScore2b = PlayerScore(players[1], 6) // 2 over
        val playerScore3b = PlayerScore(players[2], 6) // 2 over
        val playerScore4b = PlayerScore(players[3], 6) // 2 over

        skinsTracker.addScoresForHole(
            listOf(
                playerScore1b,
                playerScore2b,
                playerScore3b,
                playerScore4b
            ), 2
        )

        val playerScore1c = PlayerScore(players[0], 6) // par
        val playerScore2c = PlayerScore(players[1], 8) // 1 over
        val playerScore3c = PlayerScore(players[2], 5) // 2 over
        val playerScore4c = PlayerScore(players[3], 7) // 2 over

        skinsTracker.addScoresForHole(
            listOf(
                playerScore1c,
                playerScore2c,
                playerScore3c,
                playerScore4c
            ), 3
        )

        assertEquals(3, skinsTracker.holeResults.size)
        assertEquals(true, skinsTracker.holeResults[0].tie)
        assertEquals(2, skinsTracker.holeResults[0].winners.size)
        assertEquals(false, skinsTracker.holeResults[1].tie)
        assertEquals(1, skinsTracker.holeResults[1].winners.size)
        assertEquals(false, skinsTracker.holeResults[2].tie)
        assertEquals(1, skinsTracker.holeResults[2].winners.size)
    }

    @Test
    fun `bets tracked for players`() {
        val skinsTracker = SkinsTracker()
        val players = listOf(
            Player("player1", 0),
            Player("player2", 3),
            Player("player3", 3),
            Player("player4", 3),
        )

        val golfHolesMap = smallGolfCourse()
        val golfCourse = GolfCourse("G", golfHolesMap)

        skinsTracker.start(players, golfCourse)
        val playerScore1 = PlayerScore(players[0], 4) // 1 over
        val playerScore2 = PlayerScore(players[1], 4) // 1 over
        val playerScore3 = PlayerScore(players[2], 4) // 1 over
        val playerScore4 = PlayerScore(players[3], 6) // 2 over

        skinsTracker.addScoresForHole(
            listOf(
                playerScore1,
                playerScore2,
                playerScore3,
                playerScore4
            ), 1
        )

        val playerScore1b = PlayerScore(players[0], 4) // par
        val playerScore2b = PlayerScore(players[1], 6) // 2 over
        val playerScore3b = PlayerScore(players[2], 6) // 2 over
        val playerScore4b = PlayerScore(players[3], 6) // 2 over

        skinsTracker.addScoresForHole(
            listOf(
                playerScore1b,
                playerScore2b,
                playerScore3b,
                playerScore4b
            ), 2
        )

        val playerScore1c = PlayerScore(players[0], 6) // par
        val playerScore2c = PlayerScore(players[1], 8) // 1 over
        val playerScore3c = PlayerScore(players[2], 5) // 2 over
        val playerScore4c = PlayerScore(players[3], 7) // 2 over

        skinsTracker.addScoresForHole(
            listOf(
                playerScore1c,
                playerScore2c,
                playerScore3c,
                playerScore4c
            ), 3
        )

    }
}